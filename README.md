HTCondor python wrapper
=======================
Try it out
-----------------------
### Clone
```sh
ssh lxplus.cern.ch
git clone ssh://git@gitlab.cern.ch:7999/rmatev/pyhtc.git
cd pyhtc
```
### Submit
```sh
python test_htc.py
```
### Monitor
```sh
condor_q
condor_q -nobatch
```
### Check output
```sh
ls -l htcondor
```

References
-----------------------
- CERN guide: http://batchdocs.web.cern.ch/batchdocs/index.html
- Manual: http://research.cs.wisc.edu/htcondor/manual/v8.7.8/

"""Example script for submitting htc jobs.

In order to use a grid proxy, you need to copy it to afs
at ~/private/grid_proxy, e.g. using something like this:
    lhcb-proxy-init
    cp /tmp/x509up_u$(id -u) ~/private/grid_proxy

"""

from htc import prepare_job, submit_jobs
from os.path import expanduser

script = """
function eecho() {
  echo "$@" | tee >(cat >&2)
}

TESTING="====================== TESTING"

eecho "$TESTING arguments"
for i in "$@"; do echo $i; done
eecho "$TESTING arguments end"

eecho "$TESTING initial environment"
env | sort

eecho "$TESTING klist"
klist -f

eecho "$TESTING reading from EOS"
md5sum /eos/lhcb/wg/Luminosity/data/counters/counters-6012-195806-hlt.root

eecho "$TESTING LHCb environment"
# The --print-banner is a workaround in order not to propagate the script
# arguments to LbEnv. See https://superuser.com/questions/1029431/
. /cvmfs/lhcb.cern.ch/lib/LbEnv-stable --print-banner

eecho "$TESTING environment after group login"
env | sort

eecho "$TESTING default python version"
python --version

eecho "$TESTING lb-run"
lb-run LHCb/v42r0 python --version

eecho "$TESTING grid credentials"
lhcb-proxy-info -ddd
"""
params = {
    # "+MaxRuntime": str(10*60),
    # "RequestCpus": "1",  # RAM is 2 GB/core, i.e. 4 -> 8 GB
}

jobs = []
for i in ['a', 'b', 'c']:
    job = prepare_job(
        script,
        args=['constant', i, "test spaces in arg", "test ' quote in arg"],
        env={'HOME': expanduser('~'),
             'X509_USER_PROXY': expanduser('~/private/grid_proxy')},
        params=params,
        name='htc-test')
    jobs.append(job)
submit_jobs(jobs)

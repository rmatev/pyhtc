import datetime
import os
import sys
import subprocess
import hashlib
from collections import OrderedDict

# FIXME the python htcondor does not send credentials for some reason
#       => switch to calling condor_submit for now

# # add system python packages in case one is in the lhcb env
# sys.path.append('/usr/lib64/python2.7/site-packages') 
# import htcondor


def _hash32(x):
    return hashlib.md5(repr(x)).hexdigest()[:8]


def _mkdir_p(path):
    import os, errno
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


# def get_schedd():
#     try:
#         return get_schedd.cache
#     except AttributeError:
#         get_schedd.cache = htcondor.Schedd()
#         return get_schedd.cache


def _pack_arguments(args):
    """Prepare "new syntax" arguments.

    See http://research.cs.wisc.edu/htcondor/manual/v8.7.8/labelmancondorsubmitCondorsubmit.html

    """
    escaped = ("'" + a.replace("'", "''") + "'" for a in args)
    return '"{}"'.format(' '.join(escaped))


def _pack_environment(env):
    """Prepare "new syntax" environment.

    See http://research.cs.wisc.edu/htcondor/manual/v8.7.8/labelmancondorsubmitCondorsubmit.html#x149-109000012

    """
    escaped = ("{}='{}'".format(k, v.replace("'", "''").replace('"', '""'))
               for k, v in sorted(env.items()))
    return '"{}"'.format(' '.join(escaped))


def prepare_job(script, args=[], env={}, params={}, name='job',
                executable='htcondor/{name}-{hash}.sh',
                output_prefix='htcondor/{name}-{hash}-{now}'):

    if not script.startswith('#!'):
        script = '#!/bin/bash\n' + script

    now = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    script_hash = hash=_hash32(script)
    prefix = output_prefix.format(name=name, now=now, hash=script_hash)
    executable = executable.format(name=name, hash=script_hash)

    if 'arguments' in params:
        raise ValueError("please use args= instead of params for arguments")
    if 'environment' in params:
        raise ValueError("please use env= instead of params for environment")

    all_params = OrderedDict((
        ("JobBatchName", name),
        ("executable", executable),
        ("output", prefix + ".$(ClusterId).$(ProcId).out"),
        ("error", prefix + ".$(ClusterId).$(ProcId).err"),
        ("log", prefix + ".$(ClusterId).$(ProcId).log"),
    ))
    if args:
        all_params['arguments'] = _pack_arguments(args)
    if env:
        all_params['environment'] = _pack_environment(env)

    if set(params.keys()).intersection(all_params.keys()):
        raise ValueError('params cannot contain {}'.format(auto_params.keys()))
    all_params.update(params)

    _mkdir_p(os.path.dirname(prefix))
    _mkdir_p(os.path.dirname(executable))
    if os.path.isfile(executable):
        with open(executable) as f:
            existing = f.read()
        if script != existing:
            raise RuntimeError('existing script with the same name has '
                               'different content, this is very unlikely!')
    else:
        with open(executable, 'w') as f:
            f.write(script)

    return all_params


def submit_jobs(jobs, submit_file='htcondor/{name}-{now}-{hash}.sub'):
    now = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    names = set(j['JobBatchName'] for j in jobs)
    if len(set(j['executable'] for j in jobs)) > 1:
        raise ValueError('executable must be the same for all jobs. '
                         'Use arguments if needed')
    name = names.pop() if len(names) == 1 else 'MULTIPLE'
    jhash = _hash32(tuple(tuple(jp.items()) for jp in jobs))
    submit_file = submit_file.format(name=name, now=now, hash=jhash)
    with open(submit_file, 'w') as f:
        for job_params in jobs:
            f.write('\n'.join('{} = {}'.format(k, v) for k, v in job_params.items()))
            f.write('\nqueue\n\n')
    # sub = htcondor.Submit(all_params)
    # if transaction:
    #     cluster_id = sub.queue(transaction)
    # else:
    #     with get_schedd().transaction() as transaction:
    #         cluster_id = sub.queue(transaction)
    # TODO can ln -s the executable to a filename containing cluster_id
    # return cluster_id
    subprocess.check_call(['condor_submit', submit_file])
